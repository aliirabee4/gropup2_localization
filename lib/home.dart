// import 'package:flutter/material.dart';
// import 'package:localize_and_translate/localize_and_translate.dart';

// class HomeScreen extends StatefulWidget {
//   @override
//   _HomeScreenState createState() => _HomeScreenState();
// }

// class _HomeScreenState extends State<HomeScreen> {
//   @override
//   void initState() {
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         leading: IconButton(icon: Icon(Icons.arrow_back), onPressed: null),
//         centerTitle: true,
//         title: Text(translator.translate('login')),
//       ),
//       body: Center(
//           child: Column(
//         children: [
//           SizedBox(
//             height: 200,
//           ),
//           Text(
//             translator.translate('title'),
//             style: TextStyle(fontSize: 20),
//           ),
//           InkWell(
//             onTap: () {
//               translator.currentLanguage == 'en'
//                   ? translator.setNewLanguage(context,
//                       newLanguage: 'ar', restart: true)
//                   : translator.setNewLanguage(context,
//                       newLanguage: 'en', restart: true);
//             },
//             child: Container(
//               margin: EdgeInsets.symmetric(vertical: 50),
//               width: 100,
//               height: 50,
//               decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(5), color: Colors.teal),
//               child: Center(
//                 child: Text(
//                   translator.currentLanguage == 'en' ? 'Ar' : 'En',
//                   style: TextStyle(color: Colors.white, fontSize: 20),
//                 ),
//               ),
//             ),
//           )
//         ],
//       )),
//     );
//   }
// }
